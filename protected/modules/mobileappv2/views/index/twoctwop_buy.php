<?php
//Merchant's account information
$merchant_id = $credentials['client_id'];			//Get MerchantID when opening account with 2C2P
$secret_key = $credentials['secret_key'];	//Get SecretKey from 2C2P PGW Dashboard

//Transaction information

$order_id  = time();
$currency ='702';
$number = $amount;
$width = 10;
$padded = str_pad((string)$number, $width, "0", STR_PAD_LEFT);
$cardAmount  =$padded.'00';


//Request information
$version = "8.5";
$payment_url = "https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment";
$result_url_1 =$resultBackurl;


//Construct signature string
$params = $version.$merchant_id.$payment_description.$order_id.$currency.$cardAmount.$result_url_1;
$hash_value = hash_hmac('sha256',$params, $secret_key,false);	//Compute hash value
?>
<Div class="content_wrap normal">

<div class="header_mobile">
 <img style="height: 200px;width:100%" src="<?php echo $logo?>" />
</div>


<div class="mobile_body">
 <h3 style="text-align: left;padding-left: 10px;"><?php echo mt("Pay using 2C2P Payment")?></h3>

   <?php
   echo CHtml::hiddenField('PAY_REQUEST_ID',isset($resp['PAY_REQUEST_ID'])?$resp['PAY_REQUEST_ID']:'');
   echo CHtml::hiddenField('CHECKSUM',isset($resp['CHECKSUM'])?$resp['CHECKSUM']:'');
   ?>
   <table class="table" style="text-align:left;font-size: 30px;">

     <tr>
      <td><?php echo mt("Description")?></td>
      <td><?php echo $payment_description?></td>
     </tr>

     <?php if($fee>0):?>
     <tr>
      <td><?php echo mt("Amount")?></td>
         <td><?php echo FunctionsV3::prettyPrice( ($amount/100)-$fee  )?></td>
     </tr>
      <tr>
      <td><?php echo mt("Card fee")?></td>
          <td><?php echo FunctionsV3::prettyPrice($fee)?></td>
      </tr>
      <tr>
      <td><?php echo mt("Total")?></td>
          <td><?php echo FunctionsV3::prettyPrice( ($amount) /100)?></td>
      </tr>
     <?php else :?>
     <tr>
      <td><?php echo mt("Amount")?></td>
         <td><?php echo FunctionsV3::prettyPrice($amount/100)?></td>
     </tr>
     <?php endif;?>

       <tr>
           <td colspan="2">
               <form id="myform" method="post" action="<?=$payment_url?>">
                   <input type="hidden" name="version" value="<?=$version?>"/>
                   <input type="hidden" name="merchant_id" value="<?=$merchant_id?>"/>
                   <input type="hidden" name="currency" value="<?=$currency?>"/>
                   <input type="hidden" name="result_url_1" value="<?=$result_url_1?>"/>
                   <input type="hidden" name="hash_value" value="<?=$hash_value?>"/>
                   <input type="hidden" name="payment_description" value="<?=$payment_description?>"  readonly/><br/>
                   <input type="hidden" name="order_id" value="<?=$order_id?>"  readonly/><br/>
                   <input type="hidden" name="amount" value="<?=$cardAmount?>" readonly/><br/>
                   <button style="font-size: 25px;color: white;background-color: green" class="btn paynow_twoc_two_p"><?php echo t("Pay Now")?></button>
               </form>
           </td>
       </tr>
   </table>

</div>


<!--PRELOADER-->
<div class="main-preloader">
   <div class="inner">
   <div class="ploader"></div>
   </div>
</div>
<!--PRELOADER-->

</Div>