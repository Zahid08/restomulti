<?php
class TwoctwopController extends CController
{

	public function __construct()
	{
		Yii::app()->setImport(array(			
		  'application.components.*',
		));		
		require_once 'Functions.php';
	}
	
	public function actionIndex()
	{				
		require_once('buy.php');
		
		$device_uiid = isset($_GET['device_uiid'])?$_GET['device_uiid']:'';
		
		if(empty($error)){
            if ( $data=Yii::app()->functions->getOrder($_GET['id'])) {
                $merchant_id=isset($data['merchant_id'])?$data['merchant_id']:'';
                $client_id = $data['client_id'];
                $order_id = $data['order_id'];

                if ($credentials =$this->TwocTwopGetCredentials($merchant_id)) {
                    $success_url = websiteUrl() . "/" . APP_FOLDER . "/twoctwop/verify/?reference_id=" . urlencode($reference_id) ."&trans_type=order&clientId=".$client_id."";
                    $success_url .= "&device_uiid=" . urlencode($device_uiid);
                    try {

                        $merchant_name = isset($data['merchant_name']) ? clearString($data['merchant_name']) : '';

                        $payment_description = Yii::t("default", "Payment to merchant [merchant_name]. Order ID#[order]", array(
                            '[merchant_name]' => $merchant_name,
                            '[order]' => $_GET['id']
                        ));

                        $reference_id = $data['order_id_token'];

                        $amount_to_pay = Yii::app()->functions->normalPrettyPrice($data['total_w_tax']);
                        $reference_id = $data['order_id_token'];
                        $amount_to_pay = unPrettyPrice($amount_to_pay)*100;

                        if($merchant_id>0){
                            $logo = FunctionsV3::getMerchantLogo($merchant_id);
                        } else $logo = FunctionsV3::getDesktopLogo();

                        $this->render(APP_FOLDER.'.views.index.twoctwop_buy',array(
                            'payment_description' => $payment_description,
                            'fee' => $credentials['card_fee'],
                            'logo'=>$logo,
                            'amount' => $amount_to_pay,
                            'order_id' => $order_id,
                            'client_id' => $client_id,
                            'reference_id' => $reference_id,
                            'credentials' => $credentials,
                            'resultBackurl' => $success_url,
                        ));

                    } catch (Exception $e) {
                        $error = Yii::t("mobile2", "Caught exception: [error]", array(
                            '[error]' => $e->getMessage()
                        ));
                    }
                } else {
                    $error = mt("invalid merchant credentials");
                }
            }else{
                $error = mt("invalid merchant credentials");
            }
		}
		
		if(!empty($error)){			
			$this->redirect(Yii::app()->createUrl('/'.APP_FOLDER.'/twoctwop/error/?error='.$error ));
		}
	}

    //TwocTwoP Credentials
    public  function TwocTwopGetAdminCredentials()
    {
        $enabled = false; $mode='';
        $secret_key = ''; $publish_key='';
        $webhook_secret='';

        $enabled = getOptionA('admin_stripe_enabled');
        $mode = getOptionA('admin_stripe_mode');
        $card_fee = getOptionA('admin_stripe_card_fee');

        $mode= strtolower($mode);
        switch ($mode){
            case "live":
                $secret_key = getOptionA('admin_live_stripe_secret_key');
                $publish_key = getOptionA('admin_live_stripe_pub_key');
                $webhook_secret=getOptionA('admin_live_stripe_webhooks');
                break;

            case "sandbox":
                $secret_key = getOptionA('admin_sanbox_stripe_secret_key');
                $publish_key = getOptionA('admin_sandbox_stripe_pub_key');
                $webhook_secret=getOptionA('admin_sandbox_stripe_webhooks');
                break;
        }

        if($enabled=="yes" && !empty($secret_key) && !empty($publish_key) ){
            return array(
                'mode'=>$mode,
                'card_fee'=>$card_fee,
                'secret_key'=>$secret_key,
                'publish_key'=>$publish_key,
                'webhook_secret'=>$webhook_secret
            );
        }
        return false;
    }

    public  function TwocTwopGetCredentials($merchant_id='')
    {
        if($merchant_id<0 || empty($merchant_id)){
            return false;
        }

        $enabled = false; $mode='';
        $client_id = '';
        $secret_key='';

        if (FunctionsV3::isMerchantPaymentToUseAdmin($merchant_id)){
            $enabled = getOptionA('admin_twoctwop_enabled');
            $mode = getOptionA('admin_twoctwop_mode');
            $card_fee = getOptionA('admin_twoctwop_card_fee');

            $client_id = getOptionA('admin_twoctwop_key');
            $secret_key = getOptionA('admin_twoctwop_salt');

        } else {
            $enabled = getOption($merchant_id,'merchant_twoctwop_enabled');
            $mode = getOption($merchant_id,'merchant_twoctwop_mode');
            $card_fee = getOption($merchant_id,'merchant_twoctwop_card_fee');

            $client_id = getOption($merchant_id,'merchant_twoctwop_key');
            $secret_key = getOption($merchant_id,'merchant_twoctwop_salt');
        }

        if($enabled=='yes' && !empty($client_id) && !empty($secret_key) ){
            return array(
                'mode'=>$mode,
                'card_fee'=>$card_fee,
                'client_id'=>trim($client_id),
                'secret_key'=>trim($secret_key)
            );
        }
        return false;
    }
    //End-TwocTwoP Credentials
	
	public function actionverify()
	{
        $db=new DbExt();
        $get = $_GET;$error = '';
        $back_url = Yii::app()->createUrl('/store/confirmorder');
        $reference_id = isset($get['reference_id'])?$get['reference_id']:'';
        $trans_type = isset($get['trans_type'])?$get['trans_type']:'';
        $response = file_get_contents('php://input');

        if(!empty($reference_id) && !empty($trans_type) && !empty($response)){
            if ($data = FunctionsV3::getOrderInfoByToken($reference_id) && $_REQUEST["payment_status"]=='000'){

                $mainDataAccept = FunctionsV3::getOrderInfoByToken($reference_id);
                $merchant_id=isset($mainDataAccept['merchant_id'])?$mainDataAccept['merchant_id']:'';
                $client_id = $mainDataAccept['client_id'];
                $order_id = $mainDataAccept['order_id'];

		        if($credentials = $this->TwocTwopGetCredentials($merchant_id)) {
		           try {
		    		  if($data['status']=="paid"){
		    		  	  
		    		  	  $message = Yii::t("mobile2","payment successfull with payment reference id [ref]",array(
                            '[ref]'=>$reference_id
                          ));                                                   
						  $this->redirect(Yii::app()->createUrl('/'.APP_FOLDER.'/twoctwop/success/?message='.$message ));
						  						  
						  /*CLEAR CART*/
	                      mobileWrapper::clearCartByCustomerID($client_id);							  
		    		  	  Yii::app()->end();
		    		  	  
		    		  } else {

                          FunctionsV3::updateOrderPayment($order_id,'2C2P',
                              $_REQUEST["reference_id"],$response,$reference_id);
		    		  	     		  	 		    		  	  		    		  	  
		    		  	  /*SEND EMAIL RECEIPT*/
                          mobileWrapper::sendNotification($order_id);	
                          
                          mobileWrapper::executeAddons($order_id);
                          
                          /*CLEAR CART*/
	                      mobileWrapper::clearCartByCustomerID($client_id);	                      
                          
                          $message = Yii::t("mobile2","payment successfull with payment reference id [ref]",array(
                            '[ref]'=>$resp['id']
                          ));
                                                    
						  $this->redirect(Yii::app()->createUrl('/'.APP_FOLDER.'/twoctwop/success/?message='.$message ));
						  
		    		  	  Yii::app()->end();
		    		  }
		    		  
		           } catch (Exception $e) {		           	    
						$error = Yii::t("mobile2","Caught exception: [error]",array(
						  '[error]'=>$e->getMessage()
						));
						$raw = $e->getMessage();
						$json= json_decode($raw,true);
						if(is_array($json) && count($json)>=1){
							if(isset($json['message'])){
								$error = $json['message'];
							}
						}
				   }    
		        } else mt("invalid payment credentials");
				
			} else $error = mt("Failed getting order information");
		} else $error = mt("Sorry but we cannot find what you are looking for");
		
		if(!empty($error)){				
			 $this->redirect(Yii::app()->createUrl('/'.APP_FOLDER.'/twoctwop/error/?error='.$error ));
		}
	}
	
	public function actionsuccess()
	{
		$msg = isset($_GET['message'])?$_GET['message']:'';
		if(!empty($msg)){
			echo $msg;
		} else {
			echo mt("payment successfull");
		}
	}
	
	public function actionerror()
	{
		$error = isset($_GET['error'])?$_GET['error']:'';
		if(!empty($error)){
			echo $error;
		} else echo mt("undefined error");
	}
	
	public function actioncancel()
	{
		
	}
	
}
/*end class*/