<?php
/**
 * MerchantController Controller
 *
 */
//if (!isset($_SESSION)) { session_start(); }

class PosController extends CController
{
	public $layout='merchant_tpl';	
	public $crumbsTitle='';
	public $map_provider='';
	public $global_action_name='';
	
	public function accessRules()
	{		
		
	}
	
	public function beforeAction($action)
    {    	
    	    	
    	$action_name= $action->id ;
    	$this->global_action_name = $action_name;    	
    	$accept_controller=array('login','autologin');	    
	    if(!Yii::app()->functions->validateMerchantSession() )
	    {
	    	if (!in_array($action_name,$accept_controller)){	 	           
	           if ( Yii::app()->functions->has_session){
	    	   	    $message_out=t("You were logout because someone login with your account");
	    	   	    $this->redirect(array('merchant/login/?message='.urlencode($message_out)));
	    	   } else $this->redirect(array('merchant/login'));	           
	    	}
	    }		    
	    
	    if ( $action_name=="autologin"){
	    	return true;
	    }	   
	    	   	   	    
	    $cs = Yii::app()->getClientScript();
		$admin_decimal_place=getOptionA('admin_decimal_place');
		if (empty($admin_decimal_place)){
			$admin_decimal_place=2;
		}
		if ( $admin_decimal_place<=0){
			$admin_decimal_place=0;
		}
		$cs->registerScript(
		  'price_decimal_place',
		 "var price_decimal_place='$admin_decimal_place';",
		  CClientScript::POS_HEAD
		);
		
		$admin_decimal_separator=getOptionA('admin_decimal_separator');
		if (empty($admin_decimal_separator)){
			$admin_decimal_separator='.';
		}
		$cs->registerScript(
		  'price_decimal_separator',
		 "var price_decimal_separator='$admin_decimal_separator';",
		  CClientScript::POS_HEAD
		);
		
		$admin_thousand_separator=getOptionA('admin_thousand_separator');
		if (empty($admin_thousand_separator)){
			$admin_thousand_separator=',';
		}
		$cs->registerScript(
		  'price_thousand_separator',
		 "var price_thousand_separator='$admin_thousand_separator';",
		  CClientScript::POS_HEAD
		);
		
		
        /*ADD SECURITY VALIDATION*/
		$yii_session_token=session_id();		
		$cs->registerScript(
		  'yii_session_token',
		 "var yii_session_token='$yii_session_token';",
		  CClientScript::POS_HEAD
		);				
		$csrfTokenName = Yii::app()->request->csrfTokenName;
        $csrfToken = Yii::app()->request->csrfToken;
		$cs->registerScript(
		  "$csrfTokenName",
		 "var $csrfTokenName='$csrfToken';",
		  CClientScript::POS_HEAD
		);
		
		$image_limit_size = FunctionsV3::imageLimitSize();
		
		$cs->registerScript(
		  'image_limit_size',
		 "var image_limit_size='$image_limit_size';",
		  CClientScript::POS_HEAD
		);
		
		$cs->registerScript(
		  'current_panel',
		 "var current_panel='merchant';",
		  CClientScript::POS_HEAD
		);
		
		if (Yii::app()->functions->isMerchantLogin()){
			if(!FunctionsV3::validateMerchantControllerAccess($action_name)){	
				$this->crumbsTitle  = t("Page not found");
		    	$this->render('noaccess');
		    	Yii::app()->end();
		    }	    
		}
	    
	    return true;	    
    }	
        	
	public function init()
	{		
		
		 $name=Yii::app()->functions->getOptionAdmin('website_title');
		 if (!empty($name)){		 	
		 	 Yii::app()->name = $name;
		 }		 
		 
		 
		 $mtid=Yii::app()->functions->getMerchantID();		 
		 // set website timezone
		 $website_timezone=Yii::app()->functions->getOptionAdmin("website_timezone");		 
		 if (!empty($website_timezone)){		 	
		 	Yii::app()->timeZone=$website_timezone;
		 }		 		 
		 $mt_timezone=Yii::app()->functions->getOption("merchant_timezone",$mtid);	   	   	    	
    	 if (!empty($mt_timezone)){    	 	
    		Yii::app()->timeZone=$mt_timezone;
    	 }		     	 
    	 
    	 FunctionsV3::handleLanguage();
    	 $cs = Yii::app()->getClientScript();
    	 $lang=Yii::app()->language;
		 $cs->registerScript(
		  'lang',
		  "var lang='$lang';",
		  CClientScript::POS_HEAD
		 );
		 
		 $ajax_admin=Yii::app()->createUrl('/ajaxmerchant');
		 
		 $cs = Yii::app()->getClientScript();
		 $cs->registerScript(
		  'ajax_admin',
		  "var ajax_admin='$ajax_admin';",
		  CClientScript::POS_HEAD
		);		
		
		$cs->registerScript(
		  'ajax_url',
		  "var ajax_url='$ajax_admin';",
		  CClientScript::POS_HEAD
		);
		
		$admin_url = Yii::app()->createUrl('/admin');
		 $cs->registerScript(
		  'admin_url',
		  "var admin_url='$admin_url';",
		  CClientScript::POS_HEAD
		);
		
		$sites_url = Yii::app()->request->baseUrl;
		 $cs->registerScript(
		  'sites_url',
		  "var sites_url='$sites_url';",
		  CClientScript::POS_HEAD
		);
		
		$upload_url = Yii::app()->createUrl('/upload');
		 $cs->registerScript(
		  'upload_url',
		  "var upload_url='$upload_url';",
		  CClientScript::POS_HEAD
		);
		
	}
				  
	public function actionIndex()
	{
        if ( !Yii::app()->functions->isMerchantLogin()){
            $this->layout='login_tpl';
            $this->render('login');
        } else {
            echo "<pre>";
            print_r($_SESSION);
            exit();

            $this->crumbsTitle=Yii::t("default","Pos Operations");
            $this->render('main_screen',[

            ]);
        }

    }
	
}
/*END CONTROLLER*/