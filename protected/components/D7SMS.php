<?php
class D7SMS
{
	public $host='https://http-api.d7networks.com/send';
	public $account_key;	
	public $is_curl=false;
	public $to;
	public $dlrMethod="POST";
	public $dlrUrl='https://4ba60af1.ngrok.io/receive';
	public $dlr='yes';
	public $dlr_level='3';
	public $message;
	public $sender='';
    private $_err_msg;
    public $_smsuser='';
    public $_smspass='';
    public $_sms_url='';
    public $_smssender='smsinfo';
    public $_debug=FALSE;

    public function set_error($err=''){
        $this->_err_msg = $err;
    }

	public function sendSMS_HTTPOST($mobile_nos='',$message='')
	{
        if (substr($mobile_nos,0,1) == "+") {
            $mobile_nos = substr($mobile_nos,1,strlen($mobile_nos));
        }
        if (empty($mobile_nos)) {
            $this->set_error('Mobile number is empty');
            return false;
        }
        if (empty($message)) {
            $this->set_error('Text message is empty');
            return false;
        }
        if (strlen($mobile_nos)<=8){
            $this->set_error('Invalid mobile number.');
            return false;
        }


        $params =$this->host.'?username='.$this->_smsuser.
            '&password='.$this->_smspass.
            '&dlr-method='.$this->dlrMethod.
            '&dlr-url='.$this->dlrUrl.
            '&dlr-level='.$this->dlr_level.
            '&to='.$mobile_nos.
            '&from='.$this->_smssender.
            '&content='.rawurlencode($message);

        $resp=$this->Curl($params);
        $resp=trim($resp);

        if(!empty($resp)){
                return $resp;
        } else {
            throw new Exception(t("empty response from api"));
        }
	}

    public function Curl($param='')
	{
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL =>$param,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
	}
	
} /*END CLASS*/