<?php
$this->renderPartial('/front/banner-receipt',array(
    'h1'=>t("Payment"),
    'sub_text'=>t("")
));

$this->renderPartial('/front/order-progress-bar',array(
    'step'=>4,
    'show_bar'=>true
));

//Merchant's account information
$merchant_id = $credentials['client_id'];			//Get MerchantID when opening account with 2C2P
$secret_key = $credentials['secret_key'];	//Get SecretKey from 2C2P PGW Dashboard

//Transaction information

$order_id  = time();
$currency ='702';
$number = $amount;
$width = 10;
$padded = str_pad((string)$number, $width, "0", STR_PAD_LEFT);
$cardAmount  =$padded.'00';


//Request information
$version = "8.5";
$payment_url = "https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment";
$result_url_1 =$resultBackurl;


//Construct signature string
$params = $version.$merchant_id.$payment_description.$order_id.$currency.$cardAmount.$result_url_1;
$hash_value = hash_hmac('sha256',$params, $secret_key,false);	//Compute hash value

//$cancel_url = websiteUrl()."/merchantsignup?Do=step3b&token=$reference_id&gateway=twoctwop";
//$success_url = websiteUrl()."/stripe_success?reference_id=".urlencode($reference_id)."&trans_type=reg";

?>

<div class="sections section-grey2 section-orangeform">
    <div class="container">
        <div class="row top30">
            <div class="inner">
                <h1><?php echo t("Pay using 2C2P Payment")?></h1>

                <div class="box-grey rounded">
                    <table class="table">
                        <tr>
                        <tr>
                            <td><?php echo t("Description")?></td>
                            <td><?php echo $payment_description?></td>
                        </tr>
                        <?php if($fee>0.001):?>
                            <tr>
                                <td><?php echo t("Card Fee")?></td>
                                <td><?php echo FunctionsV3::prettyPrice($fee)?></td>
                            </tr>

                            <tr>
                                <td><?php echo t("Amount")?></td>
                                <td><?php echo FunctionsV3::prettyPrice( ($amount/100)-$fee  )?></td>
                            </tr>

                            <tr>
                                <td><?php echo t("Total")?></td>
                                <td><?php echo FunctionsV3::prettyPrice( ($amount) /100)?></td>
                            </tr>

                        <?php else :?>

                            <tr>
                                <td><?php echo t("Total")?></td>
                                <td><?php echo FunctionsV3::prettyPrice($amount/100)?></td>
                            </tr>


                        <?php endif;?>
                        <tr>
                            <td colspan="2">
                                <form id="myform" method="post" action="<?=$payment_url?>">
                                    <input type="hidden" name="version" value="<?=$version?>"/>
                                    <input type="hidden" name="merchant_id" value="<?=$merchant_id?>"/>
                                    <input type="hidden" name="currency" value="<?=$currency?>"/>
                                    <input type="hidden" name="result_url_1" value="<?=$result_url_1?>"/>
                                    <input type="hidden" name="hash_value" value="<?=$hash_value?>"/>
                                    <input type="hidden" name="payment_description" value="<?=$payment_description?>"  readonly/><br/>
                                    <input type="hidden" name="order_id" value="<?=$order_id?>"  readonly/><br/>
                                    <input type="hidden" name="amount" value="<?=$cardAmount?>" readonly/><br/>
                                    <button class="btn paynow_twoc_two_p"><?php echo t("Pay Now")?></button>
                                </form>
                            </td>
                        </tr>
                    </table>

                    <div class="top25">
                        <a href="<?php echo Yii::app()->createUrl('/store/paymentoption')?>">
                            <i class="ion-ios-arrow-thin-left"></i> <?php echo Yii::t("default","Click here to change payment option")?></a>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
