
<?php
$enabled=Yii::app()->functions->getOptionAdmin('admin_twoctwop_enabled');
$paymode=Yii::app()->functions->getOptionAdmin('admin_twoctwop_mode');
?>

<div id="error-message-wrapper"></div>

<form class="uk-form uk-form-horizontal forms" id="forms">
<?php echo CHtml::hiddenField('action','admintwoctwop')?>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Enabled 2C2P")?>?</label>
  <?php 
  echo CHtml::checkBox('admin_twoctwop_enabled',
  $enabled=="yes"?true:false
  ,array(
    'value'=>"yes",
    'class'=>"icheck"
  ))
  ?> 
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Mode")?></label>
  <?php 
  echo CHtml::radioButton('admin_twoctwop_mode',
  $paymode=="Sandbox"?true:false
  ,array(
    'value'=>"Sandbox",
    'class'=>"icheck"
  ))
  ?>
  <?php echo Yii::t("default","Sandbox")?>
  <?php 
  echo CHtml::radioButton('admin_twoctwop_mode',
  $paymode=="live"?true:false
  ,array(
    'value'=>"live",
    'class'=>"icheck"
  ))
  ?>	
  <?php echo Yii::t("default","live")?> 
</div>

    <div class="uk-form-row">
        <label class="uk-form-label"><?php echo t("Card fee")?></label>
        <?php
        echo CHtml::textField('admin_twoctwop_card_fee',
            getOption($mtid,'admin_twoctwop_card_fee')
            ,array(
                'class'=>"numeric_only"
            ))
        ?>
    </div>

    <h3><?php echo Yii::t("default","Credentials")?></h3>
<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Client ID")?></label>
  <?php 
  echo CHtml::textField('admin_twoctwop_key',
  Yii::app()->functions->getOptionAdmin('admin_twoctwop_key')
  ,array(
    'class'=>"uk-form-width-large"
  ))
  ?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Secret")?></label>
  <?php 
  echo CHtml::textField('admin_twoctwop_salt',
  Yii::app()->functions->getOptionAdmin('admin_twoctwop_salt')
  ,array(
    'class'=>"uk-form-width-large"
  ))
  ?>
</div>



<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</form>